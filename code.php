<?php
function getLetterGrade($x){
    if($x > 100){
        return "out of range";
    } if($x >= 98 && $x == 100){
        return $x." is equivalent to A+";
    } if($x >= 95 && $x < 98){
        return $x." is equivalent to A";
    } if($x >= 92 && $x < 95){
        return $x." is equivalent to A-";
    } if($x >= 89 && $x < 92){
        return $x." is equivalent to B+";
    } if($x >= 86 && $x < 89){
        return $x." is equivalent to B";
    } if($x >= 83 && $x < 86){
        return $x." is equivalent to B-";
    } if($x >= 80 && $x < 83){
        return $x." is equivalent to C+";
    } if($x >= 77 && $x < 80){
        return $x." is equivalent to C";
    } if($x >= 75 && $x < 77){
        return $x." is equivalent to C-";
    } else{
        return $x." is equivalent to D";
    }
}

?>
